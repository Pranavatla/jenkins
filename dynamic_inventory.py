import json
import subprocess

terraform_path = '/usr/local/bin/terraform'

terraform_output = subprocess.check_output([terraform_path, 'output', '-json'])
output_data = json.loads(terraform_output)

public_ip = output_data['app1_ssh_command']['value'].split('@')[-1]

inventory = {
    "all": {
        "hosts": {
            "app_server": {
                "ansible_host": public_ip,
                "ansible_user": "ubuntu",
                "ansible_ssh_private_key_file": "/Users/pranav/Pranav.pem"
            }
        }
    }
}

print(json.dumps(inventory))