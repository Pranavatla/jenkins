pipeline {
    agent any

    environment {
        ANSIBLE_HOST_KEY_CHECKING = 'False'
        PATH = "/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin"
        TF_CLI_ARGS = "-no-color"
    }

    stages {
        stage('Terraform Init') {
            steps {
                sh 'terraform init'
            }
        }

        stage('Terraform Apply') {
            steps {
                sh 'terraform apply -auto-approve'
                sh 'sleep 60'
            }
        }

        stage('Generate Dynamic Inventory and Run Ansible') {
            steps {
                script {
                    def inventory = sh(script: "python3 dynamic_inventory.py", returnStdout: true).trim()
                    writeFile file: 'inventory.json', text: inventory
                    sh 'cat inventory.json' 
                    sh 'ansible-playbook -i inventory.json deploy_docker.yml -v'
                }
            }
        }
    }

    post {
        always {
            cleanWs()
        }
        success {
            echo 'Pipeline completed successfully!'
        }
        failure {
            echo 'Pipeline failed. Please check the logs for details.'
        }
    }
}