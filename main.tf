
provider "aws" {
  region     = "ap-south-1"
  access_key = "AKIAV7D4IU4UXECTGQFA"
  secret_key = "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"
}

resource "aws_instance" "app1" {
  ami                    = "ami-03bb6d83c60fc5f7c"
  instance_type          = "t2.micro"
  key_name               = "Pranav"
  vpc_security_group_ids = ["sg-05d5516638a0e6f2d"]

  user_data = <<-EOF
                #!/bin/bash
                sudo apt update
                sudo apt install -y apt-transport-https ca-certificates curl software-properties-common nginx
                
                # Install Docker
                curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
                sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
                sudo apt update
                sudo apt install -y docker-ce
                sudo systemctl start docker
                sudo systemctl enable docker

                # Add the ubuntu user to the docker group
                sudo usermod -aG docker ubuntu
                
                # Docker image pull and container run will be handled by Ansible
                # Ansible will also configure Nginx as a reverse proxy
              EOF

  tags = {
    Name = "jenkins"
    Environment = "app1_testing"
  }
}

output "app1_ssh_command" {
  value = "Access EC2 at: ssh -o StrictHostKeyChecking=no -i /Users/pranav/Pranav.pem ubuntu@${aws_instance.app1.public_ip}"
}

output "app1_access_command" {
  value = "Wait for two minutes and Access Movie Recommender at http://${aws_instance.app1.public_ip}"
}
